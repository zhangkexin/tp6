<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
namespace app\admin\common;

use app\BaseController;
use think\App;
use app\admin\service\User;
/**
 * 控制器基础类
 */
class AdminBaseController extends BaseController
{
    protected $userinfo;
    public function initialize()
    {
        parent::initialize();

        //过滤不需要登陆的行为
       /* $allowUrl = [
            'admin/index/login',
            'admin/index/logout',
        ];

        $rule = strtolower($this->request->app() . '/' . $this->request->controller() . '/' . $this->request->action());

        if(!in_array($rule,$allowUrl)){
            if(defined('UID')){
                return;
            }
            define('UID', (int) User::instance()->isLogin());

            // 是否是超级管理员
            define('IS_ROOT', User::instance()->isAdministrator());

            if (!IS_ROOT && config('admin_allow_ip')) {
                // 检查IP地址访问
                $arr = explode(',', config('admin_allow_ip'));
                halt(config());
                foreach ($arr as $val) {
                    //是否是IP段
                    if (strpos($val, '*')) {
                        if (strpos($this->request->ip(), str_replace('.*', '', $val)) !== false) {
                            $this->error('403:你在IP禁止段内,禁止访问！');
                        }
                    } else {
                        //不是IP段,用绝对匹配
                        if ($this->request->ip() == $val) {
                            $this->error('403:IP地址绝对匹配,禁止访问！');
                        }

                    }
                }
            }
        }*/


    }



}
