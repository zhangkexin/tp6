<?php

namespace app\event;

use think\facade\Cache;
use app\admin\model\Config;
use think\facade\Config as systemConfig;
class WebConfig
{
    public $Config;
    // 事件监听
    public function __construct($Config)
    {
         $this->Config = $Config;
        // 读取系统配置
        halt($this->Config);
        $system_config = Cache("Config");

        if (empty($system_config)) {
            exit("配置读取错误！！！！");
        }

        // 设置配置信息
        if (!empty($system_config)) {
            foreach ($system_config as $key => $value) {
                systemConfig::set($key, $value);
            }
        }
    }
}
